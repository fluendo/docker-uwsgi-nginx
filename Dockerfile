FROM ubuntu:16.04

MAINTAINER fluendo

# Install required packages
RUN apt-get update && \
    apt-get upgrade -y && \ 
    apt-get install -y \
        python \
        python-dev \
        python-setuptools \
        python-pip \
        nginx \
        supervisor \
        sqlite3 && \
        pip install -U pip setuptools

# install uwsgi now because it takes a little while
RUN pip install uwsgi

# Make NGINX run on the foreground
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
# Remove default configuration from Nginx
RUN rm /etc/nginx/sites-enabled/default

# Copy the configuration
COPY etc/ /etc/

# Which uWSGI .ini file should be used, to make it customizable
ENV UWSGI_INI /app/uwsgi.ini

EXPOSE 80

# Define default command.
CMD ["bash"]
