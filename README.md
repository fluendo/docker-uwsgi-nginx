This Dockerfile allows you to build a Docker container with a fairly standard and speedy setup for Django with uWSGI and Nginx.

This project is based on
https://github.com/dockerfiles/django-uwsgi-nginx
https://github.com/tiangolo/uwsgi-nginx-docker

The next layer must add any specific packages or requirements and finaly link a specific folder for data and configuration
